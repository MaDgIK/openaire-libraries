/*
 * Public API Surface of version-update
 */

export * from './lib/version-update.service';
export * from './lib/version-update.component';
export * from './lib/version-update.module';
