import { NgModule } from '@angular/core';
import { VersionUpdateComponent } from './version-update.component';



@NgModule({
  declarations: [
    VersionUpdateComponent
  ],
  imports: [
  ],
  exports: [
    VersionUpdateComponent
  ]
})
export class VersionUpdateModule { }
