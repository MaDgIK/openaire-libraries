import * as process from "process";

const { exec } = require('child_process');

type Dependency = 'version-update';

/** Add the dependencies you want to add to your projects */
let dependencies: Dependency[] = ['version-update'];
const repository: string = 'openaire-libraries';
const gitURL: string = `https://code-repo.d4science.org/MaDgIK/${repository}.git`;

console.log('Cloning ' + repository);

function installDependency(index: number) {
  if(index < dependencies.length) {
    console.log(`Installing ${dependencies[index]}`);
    exec(`npm run build-lib --lib=${dependencies[index]}`, (error:string) => {
      if(error) {
        console.error(`Error packaging ${dependencies[index]}: ${error}`);
        clean();
        return;
      }
      process.chdir('../');
      exec(`npm install --no-save $(npm pack ./${repository}/projects/${dependencies[index]} | tail -1)`, (installError: string) => {
        if(error) {
          console.error(`Error installing ${dependencies[index]}: ${installError}`);
          clean('./');
          return;
        }
        console.log(`${dependencies[index]} has been installed successfully`);
        process.chdir(repository);
        installDependency(index + 1);
      });
    });
  } else {
    clean();
  }
}

function clean(directory: string = '../') {
  /** Clean files after packaging */
  console.log('Cleaning files')
  process.chdir(directory)
  exec(`rm -rf ${repository} package.js *.tgz`, (error: string) => {
    if(error) {
      console.error(`Error cleaning files: ${error}`);
      return;
    }
  });
}


/**
 * Clone ${repository}
 * */
exec(`git clone ${gitURL}`, (cloneError: string) => {
  if(cloneError) {
    console.error(`Error cloning the ${repository}: ${cloneError}`);
    return;
  }
  console.log(`${repository} cloned successfully.`);
  process.chdir(repository);
  /** Installing node modules */
  exec(`npm install`, (npmError: string) => {
    if(npmError) {
      console.error(`Error installing node modules: ${npmError}`);
      return;
    }
    installDependency(0);
  });
})
